# DFL IPC

Two very simple classes for IPC, especially between two instances of the same application. These classes are used in DFL::Application.


### Dependencies:
* <tt>Qt5 Core and Gui (qtbase5-dev, qtbase5-dev-tools)</tt>
* <tt>meson   (For configuring the project)</tt>
* <tt>ninja   (To build the project)</tt>


### Notes for compiling - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/desktop-frameworks/ipc.git dfl-ipc`
- Enter the `dfl-ipc` folder
  * `cd dfl-ipc`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Known Bugs
* Please test and let us know


### Upcoming
* Support for sending arbitrary data (binary data) via IPC.
* Any feature that you ask for.
